speech sources

comments on TI source:

talkx - speak external code (is this file truncated?)
talkx1 - speak external code (is this file truncated?)
  talkx1 has a 9/27/83 modification by Al Olson but is truncated before H3 and CLS
  talkx  has more equates past H2, and part of CLS routine
  Both files are truncated

talkb  - speech menu . has END statement so probably intact
talkb1 - completely lost. probably talkb revised by Al Olson

talk - includes talkx1 and talkb 

All these files were on side 1 of 2 sided floppy disk. Side 2 was erased or never copied.
Its probably my fault because the original disk had all these things working.

Prefix O indicates an assembled object file.
OTALK is from TALK, which is TALKX1 and TALKB, but probably the TALK and TALKX originally went together.

To run: LOAD AND RUN : OTALKF 
Program name is START
10 phrases are built in. Any  errors cause TALKF to say in the goofy voice "Golly, Trouble Ahead"
Press FCTN-S and type DSK1.OBUCK or any of the object files.
Most external files are in LPC mode, I'm not sure about OALLO.

The purpose (as I remember, and you can infer) was to load OTALK with an assembled phrase file.
Phrases were numbered and you could play them one at a time.

Some assembled speech phrases are in: ointe, olaso, opino, odb1, obuck
opino was for a Disney Pinocchio game or learning game.
The best phrase was "Jiminy Cricket, get me out of here!"

The goofy phrases I think escaped into the wild.

OALLO is part of Magic Wand Speaking Reader utility that drove the MWSR from the cassette
and joystick ports. MWSR has an allophone library inside, and doesn't take external LPC.
I think OALLO is intended for playback in Allophone mode on the MWSR device. DEFEAT AUTO
POWER DOWN was probably to keep the MWSR from its 3 minute power down timer.

The MWSR  scanned allophone numbers and inflection from barcodes, and spoke them much like the Text to Speech program.
A boxy device with the bar code scanner, running on a 9995 under DC power, was made by Mike Reed. 
I think it was envisioned to come with a phrase book, for use by the speech impaired. Vocaid was another
product, similar to  Touch and Tell, which had phrase pads separated by dividers.  I have seen it used
in public by a person with cerebral palsy. 




All of these show the SDSMAC assembler signature at the end of the object files. SDSMAC was a macro
assembler on the 990. The speech lab used a 990 but they had 4As for testing. You can see
evidence of encoding session numbers in the source of goofy:

* PHRASE LIBRARY 22895 CODING TABLE 10441 DATE  9/15/83  16:33:39
*  HOME COMPUTER CODING/DECODING TABLE 5200/TMS0285
*
*  INDEX   CUM BYTES      DESCRIPTION             Q/C      BITS/SEC
*     1        270    GOING UP!                    QC      1800.0

TALK is credited to Mark Rose. Al Olson and Cliff Easthom worked next to the speech lab in TI West Building,
Lubbock Texas, in 1983 they were working on Magic Wand Speaking Reader. Cliff was rumored to be the voice on 
Lasso (stated by Al Olson) but I  think the Lasso credits say otherwise.

John Philips wrote the ISRSOUND player.
--

dattack was my dump from the disk version of Demon Attack where I found an extra victory phrase
not heard in the game. unfortunately truncated, but could be recovered again.


erniebert is my dump of the popular Ernie and Bert XB demo.

notes by Erik Olson (son of Al)

