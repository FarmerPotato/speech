INTPTR EQU  >83C4        INTERRUPT ROUTINE POINTER
SWP    EQU  >8340        SOUND WORKSPACE
SPADR  EQU  SWP          CURRENT SPEECH PHRASE ADDRESS
SPLEN  EQU  1*2+SWP      CURRENT LENGTH OF SPEECH PHRASE
READIT EQU  2*2+SWP      START OF CODE (R2,3,4,5,6,7) IN FAST RAM FOR
*                        READ STATUS AND DELAY ON THE 16 BIT BUS
*
SPK    DATA SWP,SP

*
SP     LIMI 0            TURN OFF INTERRUPTS
*
*      LI   R5,>000F
*      C    @VALUE,R5
*      JEQ  CLRVAL
*
CONT   LI   R6,>2000     EQUAL STATUS BIT POSITION
       SOCB R6,R15       SET "EQU" STATUS BIT
       MOV  @R7*2(R13),R7         ; GET PARM TABLE ADDRESS
       MOV  *R7,R8       GET INTEGER TABLE ADDRESS
       MOV  @2(R8),R8    GET PARAMETER VALUE(SPEECH INDEX #, 2ND ENTRY)
       MOV  R8,R10       SAVE AS FLAG
       JLT  SPK$         CAN NOT BE NEGATIVE
       JGT  SPK1         MUST BE POSITIVE
       LI   R8,>7000     IT IS ZERO...SPEECH OFF BYTE
       MOVB R8,@SPCHWT   TURN THE PIG OFF
       CLR  @INTPTR      DISABLE SPECIAL USER INTERRUPT ROUTINE VECTOR
       RTWP              BOOGIE
SPK$   NEG  R8           MAKE POSITIVE
SPK1   CI   R8,SPTABL    TABLE LENGTH
       JGT  SERR         JUMP ON INVALID VALUE
*CLRVAL CLR  @VALUE
*      JMP  CONT
*
* READY TO INIT SPEECH
*
       SLA  R8,1         WORD ALIGN
*      A    R11,R8       ADD CURRENT LOCATION
       AI   R8,SPTAB     2 ADD IN TABLE OFFSET
       MOV  *R8,R8       GET SPEECH LIST ADDRESS
*      A    R11,R8       CORRECT IT
       MOVB *R8+,@SPLEN   1st LENGTH BYTE
       MOVB *R8+,@SPLEN+1              ; 2nd LENGTH BYTE
       NEG  @SPLEN        SET UP FIRST-TIME-ENTRY FLAG
       MOV  R8,@SPADR     INIT PHRASE ADDRESS OF SPEECH DATA
       LI   R8,SPKINT     INTERRUPT ROUTINE ADDRESS
*      A    R11,R8        CORRECT
       MOV  R8,@INTPTR    INIT USER INTERRUPT POINTER
       LI   R2,>D2A0      MOV @SPCHRD,R10(FAST RAM CODE ON 16 BIT BUS)
       LI   R3,>9000
       LI   R4,>1000      NOP(READ DATA COMMAND UNTIL DATA AVAILABLE
       MOV  R4,R5         NOP IS 12 us
       MOV  R5,R6         NOP
       LI   R7,>045B      RT
*
       MOV  R10,R10       INTERRUPT MODE ?
       JLT  SPKEX         YES...EXIT(NOW YOU CAN CLOBBER THE SPEECH)
       LIMI 2             ENABLE INTERRUPTS
SPK2   MOV  @INTPTR,R10   SPEECH WILL NOW FINISH(CAN NOT BE CLOBBER)
       JNE  SPK2          LOOP UNTIL ZERO
SPKEX  RTWP               BOOGIE
SERR   SZCB R6,R15        SET "NOT-EQUAL" STATUS
       RTWP
***
***  USER INTERRUPT ROUTINE FOR SPEECH
***  CONTROL PASSES HERE FROM INTERRUPT HANDLER
***  AND IS CURRENTLY IN GPL WORKSPACE.  R13,
***  R14, AND R15 MUST BE PRESERVED SINCE THEY
***  ARE THE WS,PC, AND STATUS REG VALUES FOR
***  THE INTERRUPT WORKSPACE.
***
***
***   REGISTER USAGE
***
*** R0 - R5   FREE
*** R6        TEMPORARY
*** R7        TEMPORARY
*** R8        ADDRESS POINTER TO LPC DATA
*** R9        TEMPORARY
*** R10       DESTINATION REGISTER FOR SPEECH STATUS INFO
*** R11       RETURN LINKAGE
*** R12       FREE
***

INTWS  EQU  >83C0             INTERRUPT WORK SPACE
SPCHRD EQU  >9000             READ  SPEECH MEMORY MAPPED ADDRESS
SPCHWT EQU  >9400             WRITE SPEECH MEMORY MAPPED ADDRESS
       EVEN
*
SPKINT MOV  @SPADR,R8         AFFECT STATUS W/ADDRESS POINTR
       JEQ  SPKRT3            IF NO POINTER, EXIT
       LI   R9,8              DEFAULT NO. OF BYTES TO MOVE
       MOV  @SPLEN,R6         FIRST TIME THROUGH?
       JGT  GRMFIX            NO, SO DO NOT SET UP INITS
       NEG  @SPLEN            YES, SO DO INITS - MAKE SPEECH PHR LEN +
***
***   LOAD SPEECH ADDRESS TO LOOK AT >0000 IN PHRASE ROM
***   TO SEE IF SPEECH SYNTHESIZER IS ATTACHED
***
       CLR  R0                LOOK AT >0000 FIRST(LOAD THIS ADDRESS)
       LI   R2,4              GO THROUGH 4 NYBBLES TO LOAD
LOADLP SRC  R0,4              LOOK AT EACH NYBBLE(START W/ LSnybble
       MOV  R0,R1             SAVE IT OFF
       SRC  R1,4              GET THAT NYBBLE
       ANDI R1,>0F00          ONLY
       ORI  R1,>4000          SET TO >4X00 FORMAT
       MOVB R1,@SPCHWT        WRITE THAT NYBBLE
       DEC  R2                NEXT NYBBLE, IF NOT ZERO
       JNE  LOADLP            NOT THROUGH YET
       LI   R4,>4000          FIFTH NYBBLE MUST BE ZERO(ALSO >4X00 FORMAT)
       MOVB R4,@SPCHWT        WRITE IT NOW
       LI   R1,10             TIME DELAY 42 us
DEL42  DEC  R1                FROM LOADING ADR
       JNE  DEL42             UNTIL NEXT COMMAND
       LI   R6,>1000          GET BYTE
       MOVB R6,@SPCHWT        READ DATA COMMAND
       NOP                    TIME DELAY 12 us
       NOP                    FROM READ DATA/STATUS
       NOP                    UNTIL DATA AVAILABLE
       LI   R6,>AA00          GET BYTE
       BL   @READIT           CHECK SPEECH CHIP STATUS(CODE ON 16 BIT BUS)
       CB   R10,R6            IS IT >AA?(SPEECH SYNTHESIZER ATTACHED?)
       JEQ  SPCMND            YES, SO LET'S DO SOME SPEAK'N
       CLR  @SPLEN            NO,  HOOK UP THE SYNTHESIZER, DUMMY
       JMP  SPKRET            CLEAN UP AND EXIT(IT WON'T SAY A THING)
SPCMND LI   R6,>6000          GET BYTE(NOW PHRASE DATA CAN BE LOADED
       MOVB R6,@SPCHWT        SEND THE 'SPEAK EXTERNAL' CMND
       NOP                    10 us DELAY BEFORE NEXT ACCESS
       NOP                    AFTER A WRITE TO PHRASE ROM
       NOP
       NOP
       LI   R9,16             INIT BYTE COUNT TO FILL 16 BYTE SPEECH FIFO
       JMP  SPCHST            CHECK SPEECH STATUS
GRMFIX LI   R9,8              8 BYTES TO SEND TO SPEECH FIFO
SPCHST BL   @READIT           READ SPEECH CHIP STATUS
       SLA  R10,2             MOV 2ND MSBIT INTO CARRY BIT
       JNC  SPKRET            IF BUFFER NOT LOW, JUMP
       MOV  R8,R10            SET ROM ADDRESS FOR NEXT SPEECH BYTE
*
LOOPR  MOVB *R10+,@SPCHWT     MOVE ONE BYTE TO SPEECH FIFO(WAS MOVB FROM GRMWA
       DEC  @SPLEN            DECREMENT TOTAL BYTE COUNTER
       JEQ  SPKRET            IF ZERO, JUMP TO EXIT
       INC  R8                POINT TO NEXT SPEECH BYTE IN ROM
       DEC  R9                DECREMENT LOCAL FIFO FILL CNT
       JNE  LOOPR             JUMP AND MOVE NXT BYTE IF NOT ZERO
       JMP  SPKRT3            PREVIOUSLY JMP TO SPKRT2
*
SPKRET MOV  @SPLEN,R7         CHECK FOR ZERO VALUE IN LENGTH
       JNE  SPKRT3            IF LENGTH NOT ZERO, RESTORE PNTR
       CLR  R8                CLEAR POINTER VAL; USE AS FLAG
       CLR  @INTPTR           CLEAR VECTOR FOR INTERRUPT:NO USER INTERRUPT
SPKRT3 MOV  R8,@SPADR         RESTORE CURRENT POINTER VALUE
       LWPI INTWS             RESTORE INTERRUPT LINKAGE
       RTWP                   RETURN TO CALLING ASMBLY PRGRM
*
SPTAB DATA 0000
      DATA SP1            1 OK
      DATA SP2            2 GREAT
      DATA SP3            3 DOING FINE
      DATA SP4            4 THAT'S RIGHT
      DATA SP5            5 GOOD
      DATA SP6            6 VERY GOOD
      DATA SP7            7 RIGHT
      DATA SP8            8 FINE
      DATA SP9            9 GOOD JOB
      DATA SP10          10 EXCELLENT
      DATA SP11          11 SUPER
      DATA SP12          12 NO
      DATA SP13          13 TRY AGAIN
      DATA SP14          14 WELCOME TO PLATO(ENGLISH)
      DATA SP15          15 GOOD DAY(ENGLISH)
SPTABL EQU  $-SPTAB/2
*     OK
SP1   DATA  183
      BYTE  >C5,>D4,>F9,>25,>C4,>36,>AD,>52
      BYTE  >E5,>F5,>10,>DE,>72,>4A,>15,>3B
      BYTE  >2A,>25,>E9,>29,>64,>ED,>C8,>54
      BYTE  >B7,>23,>97,>A5,>83,>C5,>FC,>9A
      BYTE  >C2,>BB,>2E,>27
      BYTE  >A5,>04,>01,>C8,>55,>2A,>01,>25
      BYTE  >B1,>1A,>80,>73,>F1,>E3,>47,>67
      BYTE  >9A,>3E,>B6,>4E,>18,>8D,>7B,>46
      BYTE  >2F,>39,>51,>0C,>6E,>91,>33,>FB
      BYTE  >84,>D1,>87,>45
      BYTE  >4D,>ED,>13,>44,>17,>16,>35,>6B
      BYTE  >4E,>90,>8D,>7B,>65,>B5,>3B,>61
      BYTE  >B6,>AE,>91,>BD,>F6,>24,>29,>B8
      BYTE  >44,>F6,>9C,>53,>66,>91,>92,>D7
      BYTE  >0D,>4F,>DB,>54
      BYTE  >70,>74,>CE,>5A,>7D,>B3,>89,>DE
      BYTE  >35,>B3,>4C,>35,>39,>54,>EC,>48
      BYTE  >02,>08,>A5,>85,>00,>AD,>7C,>DC
      BYTE  >16,>B4,>8A,>BB,>D1,>71,>BB,>E5
      BYTE  >A6,>AE,>E9,>C4
      BYTE  >6D,>5A,>86,>07,>AB,>62,>B3,>6B
      BYTE  >99,>E9,>EC,>74,>54,>1E,>98,>59
      BYTE  >8A,>C9,>51,>65,>10,>AA,>26,>E1
      BYTE  >D6,>8C,>49,>B9,>39,>B9,>6D,>35
      BYTE  >46,>2E,>E6,>14
      BYTE  >AE,>E0,>01,>00
*     GREAT
SP2   DATA  101
      BYTE  >21,>89,>7D,>AA,>D4,>95,>A4,>C4
      BYTE  >DF,>2D,>33,>57,>53,>12,>3D,>9C
      BYTE  >33,>D4,>DB,>28,>F4,>31,>1E,>97
      BYTE  >AC,>A7,>30,>2D,>CD,>22,>D2,>9E
      BYTE  >DC,>97,>52,>B3
      BYTE  >48,>7E,>F2,>18,>4A,>2C,>23,>CF
      BYTE  >C9,>B3,>0B,>B5,>89,>CE,>A7,>C8
      BYTE  >C9,>59,>3B,>57,>9F,>3A,>47,>23
      BYTE  >EB,>5A,>34,>FA,>E4,>15,>73,>3B
      BYTE  >76,>99,>73,>70
      BYTE  >F0,>AF,>C4,>CA,>6A,>C6,>C9,>D4
      BYTE  >9D,>00,>18,>A0,>9B,>44,>05,>14
      BYTE  >4B,>C8,>00,>2B,>45,>08,>E0,>AC
      BYTE  >28,>02,>14,>4F,>7D,>00
*     CONGRATULATIONS
SP3   DATA  254
      BYTE  >49,>B1,>5E,>3D,>22,>A9,>14,>52
      BYTE  >06,>8B,>CC,>42,>52,>34,>63,>BD
      BYTE  >D3,>02,>49,>D0,>94,>F1,>53,>6A
      BYTE  >97,>4E,>E3,>3E,>C7,>29,>63,>21
      BYTE  >00,>77,>D7,>A4
      BYTE  >E9,>10,>16,>96,>89,>9B,>E6,>1A
      BYTE  >97,>67,>49,>19,>AA,>2F,>DA,>99
      BYTE  >23,>FB,>A8,>21,>59,>56,>AE,>92
      BYTE  >A3,>C6,>68,>91,>79,>8A,>93,>92
      BYTE  >82,>A9,>4F,>29
      BYTE  >51,>8A,>E3,>E2,>69,>E1,>D4,>01
      BYTE  >83,>B3,>05,>E5,>8E,>04,>36,>6D
      BYTE  >5B,>94,>6C,>38,>3D,>DB,>68,>51
      BYTE  >82,>B1,>B0,>8E,>C8,>45,>8D,>C6
      BYTE  >5D,>A3,>63,>17
      BYTE  >C5,>EB,>34,>8B,>93,>D3,>14,>67
      BYTE  >D3,>7D,>47,>E9,>52,>A2,>0F,>B3
      BYTE  >2E,>C5,>4B,>49,>DE,>CD,>37,>94
      BYTE  >2C,>39,>3B,>75,>BF,>94,>3B,>E4
      BYTE  >62,>35,>BC,>5B
      BYTE  >4A,>51,>86,>62,>DB,>08,>D1,>01
      BYTE  >38,>BD,>34,>00,>7F,>AC,>07,>E0
      BYTE  >AF,>D2,>00,>FC,>35,>51,>F4,>BF
      BYTE  >7E,>33,>54,>59,>D2,>BB,>30,>2D
      BYTE  >AD,>CA,>C9,>F0
      BYTE  >D6,>C3,>B3,>19,>05,>CB,>3A,>4F
      BYTE  >8B,>A4,>E3,>EC,>20,>B4,>8A,>5C
      BYTE  >B5,>B1,>83,>D4,>4D,>76,>C5,>C6
      BYTE  >8D,>C2,>6A,>30,>54,>9B,>D6,>49
      BYTE  >DD,>E2,>70,>EC
      BYTE  >7A,>CF,>75,>86,>CD,>8D,>2A,>22
      BYTE  >97,>68,>75,>27,>42,>9F,>D6,>C4
      BYTE  >D4,>65,>13,>A0,>8A,>34,>04,>7C
      BYTE  >95,>86,>80,>2F,>43,>11,>F0,>85
      BYTE  >05,>02,>BE,>4E
      BYTE  >87,>07
*     THAT'S RIGHT
SP4   DATA  208
      BYTE  >A1,>1C,>31,>3C,>CC,>95,>AE,>2A
      BYTE  >6B,>8D,>F4,>AE,>75,>EA,>A2,>A5
      BYTE  >DA,>BB,>E1,>A9,>8A,>D1,>CC,>E8
      BYTE  >99,>A7,>CC,>D6,>32,>72,>6A,>9D
      BYTE  >3C,>07,>4B,>CF
      BYTE  >9E,>7D,>B2,>1C,>3C,>23,>EA,>CA
      BYTE  >49,>52,>F0,>74,>EF,>29,>27,>0A
      BYTE  >21,>D2,>BC,>EB,>A4,>C8,>D9,>2C
      BYTE  >B3,>8E,>8D,>00,>27,>CC,>0C,>90
      BYTE  >4D,>BA,>03,>BE
      BYTE  >B1,>48,>C0,>2F,>A9,>0E,>F8,>B3
      BYTE  >DC,>00,>B5,>85,>22,>40,>3B,>65
      BYTE  >04,>28,>6D,>AA,>C2,>A4,>DC,>5D
      BYTE  >4C,>8D,>09,>95,>EE,>34,>4E,>A5
      BYTE  >2E,>94,>B9,>55
      BYTE  >A4,>E5,>05,>4F,>AD,>C0,>F0,>50
      BYTE  >96,>1C,>7D,>85,>C7,>4D,>7B,>F1
      BYTE  >F4,>15,>7D,>D1,>6E,>CB,>33,>B3
      BYTE  >D4,>BD,>DD,>9F,>D0,>D6,>49,>B3
      BYTE  >4A,>76,>52,>97
      BYTE  >37,>CD,>AA,>E9,>C9,>7C,>DE,>E0
      BYTE  >AC,>BA,>27,>0F,>71,>5C,>2B,>1B
      BYTE  >9F,>22,>BA,>72,>9F,>A8,>7D,>AA
      BYTE  >64,>93,>63,>3A,>D1,>6A,>0A,>77
      BYTE  >CB,>ED,>8A,>A5
      BYTE  >6B,>CA,>49,>6F,>2A,>85,>21,>47
      BYTE  >03,>EF,>8E,>0F,>00,>06,>C8,>2A
      BYTE  >54,>01,>35,>3A,>0B,>A0,>04,>09
      BYTE  >06,>44,>A3,>FE
*     GOOD
SP5   DATA  130
      BYTE  >A9,>48,>2B,>2C,>24,>9C,>8C,>22
      BYTE  >05,>63,>CD,>C8,>73,>AA,>10,>54
      BYTE  >23,>BC,>D7,>29,>92,>35,>F5,>0C
      BYTE  >3F,>27,>4B,>D6,>CC,>D3,>F3,>9E
      BYTE  >24,>38,>37,>0F
      BYTE  >CF,>73,>92,>E4,>C3,>C5,>35,>EF
      BYTE  >49,>62,>08,>57,>D3,>3E,>27,>49
      BYTE  >CE,>DD,>42,>F7,>9E,>3C,>F9,>70
      BYTE  >75,>EB,>7D,>2A,>E7,>CD,>C3,>6B
      BYTE  >E3,>69,>23,>8F
      BYTE  >F0,>CC,>0C,>A7,>2F,>D2,>CD,>A3
      BYTE  >7A,>AE,>7E,>04,>57,>21,>E9,>62
      BYTE  >46,>CF,>55,>43,>CB,>8D,>9B,>22
      BYTE  >17,>75,>2D,>37,>6A,>AA,>D2,>55
      BYTE  >84,>15,>23,>C0
      BYTE  >51,>F7,>32,>04,>ED,>12,>FE,>B1
      BYTE  >C3,>D0,>B3,>8B,>41,>D4,>31,>FD
      BYTE  >08,>69,>41,>6A,>EB,>01
*     NICE
SP6   DATA  72
      BYTE  >46,>E3,>B2,>27,>24,>14,>37,>CD
      BYTE  >47,>0F,>5F,>95,>73,>B4,>18,>3B
      BYTE  >3C,>DA,>CE,>D5,>AE,>76,>D4,>14
      BYTE  >33,>2C,>5B,>F1,>51,>4A,>76,>D7
      BYTE  >2D,>D9,>4B,>2E
      BYTE  >C1,>DC,>D6,>A5,>0E,>A9,>3A,>09
      BYTE  >7F,>57,>D2,>84,>62,>38,>A2,>82
      BYTE  >56,>E2,>B3,>65,>93,>4F,>39,>8E
      BYTE  >4F,>C2,>38,>6B,>9D,>30,>E0,>EB
      BYTE  >0C,>0A,>50,>80
*     RIGHT
SP7   DATA  130
      BYTE  >AA,>6D,>3A,>D4,>D5,>6D,>9B,>36
      BYTE  >CA,>70,>B7,>50,>6C,>56,>65,>AA
      BYTE  >42,>4D,>89,>19,>A4,>9D,>70,>76
      BYTE  >25,>AE,>97,>B6,>C3,>39,>54,>BB
      BYTE  >5E,>C4,>51,>93
      BYTE  >74,>12,>3A,>D9,>8A,>D5,>DB,>76
      BYTE  >6A,>F5,>25,>5F,>95,>6C,>AB,>32
      BYTE  >CB,>74,>25,>FD,>9C,>CA,>96,>0A
      BYTE  >D7,>CA,>76,>4A,>1F,>BB,>5D,>23
      BYTE  >DD,>49,>7D,>9E
      BYTE  >56,>CD,>66,>27,>09,>7E,>4A,>23
      BYTE  >97,>9E,>24,>B9,>49,>C9,>6A,>72
      BYTE  >D2,>14,>DA,>A4,>B3,>C9,>29,>52
      BYTE  >0A,>F5,>CC,>B9,>A7,>2A,>36,>C4
      BYTE  >A6,>6B,>9F,>B6
      BYTE  >28,>55,>EF,>AE,>C5,>00,>C5,>22
      BYTE  >19,>60,>A4,>28,>00,>80,>01,>9A
      BYTE  >4A,>51,>40,>8F,>C6,>0F
*     FINE
SP8   DATA  165
      BYTE  >08,>C8,>26,>8D,>00,>D9,>B9,>11
      BYTE  >C0,>EB,>70,>02,>64,>63,>D6,>52
      BYTE  >67,>A3,>97,>D5,>C9,>A9,>6C,>E9
      BYTE  >0E,>A8,>CC,>A7,>74,>65,>3B,>28
      BYTE  >D3,>9C,>C2,>E5
      BYTE  >2D,>D3,>88,>77,>72,>57,>AE,>C4
      BYTE  >23,>DE,>C9,>5C,>BD,>E4,>88,>B8
      BYTE  >27,>75,>E3,>82,>33,>EC,>9E,>D4
      BYTE  >8F,>71,>49,>8B,>77,>52,>3F,>DA
      BYTE  >25,>3D,>DE,>C9
      BYTE  >42,>ED,>90,>B4,>B8,>27,>0F,>3D
      BYTE  >5D,>C3,>9D,>9C,>2A,>E6,>34,>0D
      BYTE  >8F,>7B,>EA,>EC,>D2,>25,>3D,>F2
      BYTE  >69,>9B,>F1,>90,>B4,>D0,>A7,>5B
      BYTE  >DE,>9C,>D3,>23
      BYTE  >AD,>7E,>46,>57,>0A,>AB,>34,>FA
      BYTE  >19,>4D,>39,>2D,>F6,>18,>87,>11
      BYTE  >D5,>12,>3B,>69,>CA,>CA,>49,>D2
      BYTE  >9C,>B8,>29,>7A,>92,>B0,>70,>66
      BYTE  >C6,>20,>4C,>DD
      BYTE  >C2,>9D,>6B,>AB,>76,>55,>71,>3B
      BYTE  >AE,>6F,>2E,>49,>CC,>AD,>98,>AE
      BYTE  >D9,>60,>13,>A3,>FD,>00
*     GOOD JOB
SP9   DATA  197
      BYTE  >01,>48,>85,>A3,>E5,>B9,>18,>71
      BYTE  >46,>EB,>93,>E7,>E8,>2C,>69,>9D
      BYTE  >4E,>1E,>93,>B1,>B9,>6F,>3A,>59
      BYTE  >2A,>A6,>26,>BE,>E6,>24,>25,>BA
      BYTE  >AA,>EA,>96,>15
      BYTE  >B5,>1C,>E2,>C4,>5D,>4D,>68,>78
      BYTE  >A6,>47,>98,>52,>91,>61,>11,>69
      BYTE  >61,>53,>C5,>8E,>BA,>A5,>65,>62
      BYTE  >91,>16,>AD,>AA,>10,>6E,>41,>00
      BYTE  >55,>9A,>07,>E0
      BYTE  >26,>D5,>06,>0C,>CF,>78,>8A,>AE
      BYTE  >5D,>D9,>72,>D3,>29,>B2,>4A,>B6
      BYTE  >CA,>D6,>27,>73,>C9,>2D,>63,>B3
      BYTE  >9E,>DC,>C7,>32,>8F,>68,>7B,>72
      BYTE  >17,>C7,>C2,>A6
      BYTE  >ED,>C9,>5C,>A9,>34,>AF,>2C,>27
      BYTE  >73,>B9,>D2,>BC,>BA,>9C,>DC,>E5
      BYTE  >49,>B3,>EA,>72,>0A,>97,>C7,>43
      BYTE  >AB,>CD,>A9,>5D,>3A,>B1,>CC,>C6
      BYTE  >A7,>73,>F6,>2D
      BYTE  >CD,>1E,>AE,>DE,>CB,>8F,>74,>59
      BYTE  >D8,>7A,>A7,>DF,>D2,>EC,>66,>19
      BYTE  >AD,>3B,>4D,>8D,>C7,>69,>76,>6A
      BYTE  >DD,>D8,>93,>A8,>21,>71,>13,>63
      BYTE  >73,>2A,>86,>11
      BYTE  >52,>43,>8C,>8A,>3B,>83,>4D,>73
      BYTE  >A9,>A4,>66,>6A,>BE,>3C,>40,>ED
      BYTE  >3C,>00
*     EXCELLENT
SP10  DATA  146
      BYTE  >A7,>AA,>36,>55,>A3,>E7,>9C,>32
      BYTE  >FB,>50,>CF,>69,>7C,>B2,>E4,>42
      BYTE  >33,>BA,>E1,>49,>A2,>49,>8D,>CE
      BYTE  >C6,>27,>4A,>D5,>B1,>22,>1A,>23
      BYTE  >80,>C9,>4C,>04
      BYTE  >24,>ED,>6E,>80,>60,>0B,>03,>70
      BYTE  >6C,>48,>00,>8E,>09,>0D,>40,>E7
      BYTE  >4A,>01,>68,>42,>21,>00,>4D,>2A
      BYTE  >9D,>2C,>2A,>AF,>16,>5D,>7D,>4A
      BYTE  >2F,>7B,>9C,>F5
      BYTE  >55,>6B,>94,>9D,>73,>D4,>57,>A5
      BYTE  >51,>6E,>4E,>51,>6F,>B7,>46,>FB
      BYTE  >1E,>45,>7F,>B4,>1A,>EB,>27,>8D
      BYTE  >73,>E5,>6A,>23,>9F,>6A,>B1,>06
      BYTE  >AB,>4F,>B2,>22
      BYTE  >CC,>23,>B5,>BE,>C8,>72,>1D,>75
      BYTE  >51,>D6,>AC,>5A,>D4,>CD,>B1,>72
      BYTE  >A2,>52,>71,>73,>A5,>00,>00,>06
      BYTE  >48,>BC,>52,>01,>55,>0B,>0A,>20
      BYTE  >30,>26,>02,>78
      BYTE  >A5,>FE
*     SORRY
SP11  DATA  72
      BYTE  >06,>F8,>DA,>C3,>02,>1E,>68,>92
      BYTE  >09,>19,>96,>C5,>64,>48,>7A,>54
      BYTE  >A6,>35,>DC,>25,>9B,>DE,>91,>B6
      BYTE  >76,>97,>62,>56,>46,>FA,>DA,>1D
      BYTE  >8A,>19,>51,>E1
      BYTE  >63,>6D,>A8,>B6,>7A,>BB,>8F,>95
      BYTE  >A6,>D9,>6A,>15,>19,>51,>9B,>EE
      BYTE  >B3,>46,>64,>94,>19,>46,>4C,>1A
      BYTE  >9E,>63,>65,>18,>39,>A8,>67,>8C
      BYTE  >95,>66,>54,>A7
*     NO
SP12  DATA  138
      BYTE  >AE,>55,>32,>DD,>25,>13,>BB,>D6
      BYTE  >09,>9A,>D2,>70,>E2,>1A,>25,>74
      BYTE  >4A,>42,>96,>6B,>84,>8A,>72,>2D
      BYTE  >CB,>AB,>CD,>D6,>8D,>35,>22,>9F
      BYTE  >26,>C4,>52,>CD
      BYTE  >8E,>7D,>9A,>90,>2A,>39,>32,>D1
      BYTE  >69,>42,>CC,>11,>8B,>56,>A7,>89
      BYTE  >2E,>D6,>5C,>3B,>9D,>26,>F8,>38
      BYTE  >35,>ED,>75,>9A,>E0,>FD,>DC,>A4
      BYTE  >CF,>69,>BC,>F3
      BYTE  >0B,>55,>BF,>A7,>71,>3E,>CF,>C5
      BYTE  >FC,>9E,>C6,>FA,>DC,>60,>F3,>77
      BYTE  >6A,>EB,>6B,>9C,>DC,>DF,>29,>AD
      BYTE  >ED,>0A,>76,>6F,>27,>B7,>3A,>27
      BYTE  >45,>F4,>B5,>4C
      BYTE  >D9,>9E,>60,>C9,>9B,>62,>11,>AE
      BYTE  >98,>DD,>8B,>8B,>84,>E9,>4C,>22
      BYTE  >35,>2A,>D4,>3C,>22,>D4,>E4,>AA
      BYTE  >30,>29,>0F,>17,>95,>F4
*     TRY AGAIN
SP13  DATA  240
      BYTE  >09,>E8,>DE,>C2,>01,>D9,>BB,>18
      BYTE  >20,>78,>13,>06,>88,>A0,>5A,>2A
      BYTE  >B5,>95,>2F,>C4,>FB,>A8,>D4,>76
      BYTE  >3A,>13,>F7,>A7,>D6,>BD,>2C,>AC
      BYTE  >9C,>9D,>DA,>D4
      BYTE  >89,>90,>4E,>7B,>6A,>9B,>37,>9C
      BYTE  >3B,>DB,>A9,>6D,>9E,>74,>E9,>2C
      BYTE  >A7,>76,>79,>52,>23,>9B,>9C,>DA
      BYTE  >95,>F2,>F0,>6E,>75,>2A,>1F,>D2
      BYTE  >A3,>63,>E1,>A9
      BYTE  >A2,>0D,>F7,>A9,>D9,>A7,>4C,>C1
      BYTE  >D5,>BA,>17,>9D,>22,>79,>57,>AF
      BYTE  >5A,>74,>8A,>14,>4C,>A2,>6A,>D5
      BYTE  >29,>72,>36,>F6,>CC,>D5,>A5,>CC
      BYTE  >93,>30,>23,>12
      BYTE  >A9,>42,>71,>8F,>F2,>8C,>AD,>0A
      BYTE  >4B,>CD,>C3,>CA,>4D,>2B,>67,>F1
      BYTE  >50,>73,>CA,>AB,>9C,>91,>35,>CC
      BYTE  >1D,>9F,>B8,>5A,>51,>AD,>99,>73
      BYTE  >E2,>9E,>4D,>D9
      BYTE  >6B,>F6,>89,>7A,>71,>91,>C8,>59
      BYTE  >27,>1A,>35,>44,>3C,>6A,>9D,>B0
      BYTE  >E7,>10,>F1,>A8,>74,>C2,>96,>43
      BYTE  >24,>A2,>F4,>89,>6A,>0C,>95,>F0
      BYTE  >D2,>27,>2E,>3E
      BYTE  >4C,>23,>23,>9D,>34,>87,>34,>8D
      BYTE  >A8,>7D,>F2,>EC,>CB,>D4,>33,>D1
      BYTE  >29,>B2,>2F,>15,>CF,>44,>AD,>0C
      BYTE  >C6,>4D,>59,>D3,>A6,>C2,>59,>4D
      BYTE  >E1,>48,>1B,>72
      BYTE  >AD,>B4,>42,>D2,>AD,>C9,>2C,>37
      BYTE  >0F,>4D,>77,>2E,>CB,>C6,>D4,>C5
      BYTE  >65,>A9,>AC,>9B,>10,>45,>93,>FC
*     WELCOME TO TI PLATO  (ENGLISH)
SP14  DATA  460
      BYTE  >43,>F7,>7E,>52,>B2,>D8,>34,>4D
      BYTE  >87,>2D,>E9,>66,>9C,>34,>5E,>DA
      BYTE  >6C,>42,>4D,>D0,>58,>19,>B7,>4D
      BYTE  >29,>41,>E3,>79,>DC,>3A,>24,>27
      BYTE  >8D,>A7,>36,>EB
      BYTE  >A2,>BD,>54,>A3,>3A,>25,>5B,>D1
      BYTE  >52,>BD,>EE,>D0,>38,>25,>4B,>F5
      BYTE  >2E,>DD,>63,>E4,>0C,>D5,>07,>F7
      BYTE  >B4,>B1,>DD,>34,>9F,>D5,>CB,>47
      BYTE  >72,>31,>42,>11
      BYTE  >6F,>1D,>DB,>CD,>8C,>85,>23,>6D
      BYTE  >6D,>03,>00,>02,>12,>53,>77,>40
      BYTE  >66,>9E,>02,>A8,>4C,>15,>00,>00
      BYTE  >00,>00,>00,>00,>10,>90,>B9,>19
      BYTE  >00,>C0,>D0,>8B
      BYTE  >B5,>C8,>75,>39,>CB,>28,>5E,>3D
      BYTE  >B7,>19,>2F,>3D,>69,>CD,>6C,>97
      BYTE  >BB,>B4,>68,>A4,>6A,>4C,>C9,>52
      BYTE  >BD,>D5,>CC,>75,>3B,>4B,>75,>D6
      BYTE  >C3,>2F,>EC,>0C
      BYTE  >45,>BB,>8C,>D8,>92,>3D,>14,>A7
      BYTE  >A3,>AC,>5B,>F6,>50,>A2,>71,>97
      BYTE  >6B,>5A,>43,>2E,>46,>DD,>B6,>18
      BYTE  >2D,>B9,>1A,>31,>AF,>92,>35,>E4
      BYTE  >E6,>D8,>7C,>8A
      BYTE  >F6,>52,>8A,>11,>CF,>29,>DA,>4B
      BYTE  >C9,>46,>23,>27,>E8,>2C,>25,>6A
      BYTE  >CB,>6C,>A3,>9D,>14,>2B,>EC,>53
      BYTE  >94,>75,>52,>8C,>F4,>77,>61,>D7
      BYTE  >49,>51,>C6,>5F
      BYTE  >59,>5D,>25,>45,>98,>EE,>24,>95
      BYTE  >63,>14,>C9,>B2,>DB,>CD,>35,>04
      BYTE  >20,>34,>37,>07,>F8,>62,>6E,>80
      BYTE  >58,>53,>8A,>92,>6C,>53,>86,>58
      BYTE  >5A,>9A,>2D,>56
      BYTE  >65,>2D,>E7,>68,>2E,>46,>A7,>0F
      BYTE  >DB,>A3,>BB,>18,>15,>59,>6C,>8F
      BYTE  >EE,>43,>54,>64,>31,>3B,>BA,>8F
      BYTE  >99,>E9,>CD,>EC,>68,>2E,>66,>85
      BYTE  >0F,>D3,>A1,>79
      BYTE  >1F,>EB,>56,>48,>08,>C0,>89,>87
      BYTE  >02,>32,>17,>56,>40,>36,>24,>E0
      BYTE  >80,>2C,>CC,>19,>C0,>4A,>15,>00
      BYTE  >38,>C0,>69,>CF,>A1,>07,>1B,>59
      BYTE  >EB,>8C,>9B,>1E
      BYTE  >B4,>67,>8C,>33,>76,>66,>A0,>9A
      BYTE  >65,>4B,>CB,>99,>51,>70,>46,>A6
      BYTE  >65,>63,>7A,>25,>9A,>39,>92,>8D
      BYTE  >15,>A4,>68,>44,>45,>56,>56,>E6
      BYTE  >A2,>E1,>E6,>5A
      BYTE  >19,>85,>B3,>86,>86,>D3,>66,>44
      BYTE  >AB,>19,>59,>8C,>97,>E1,>4D,>4C
      BYTE  >4E,>31,>3E,>7A,>32,>51,>59,>C1
      BYTE  >7A,>E9,>C1,>47,>54,>39,>EB,>A1
      BYTE  >BB,>18,>51,>19
      BYTE  >68,>8A,>6E,>7C,>B5,>85,>B1,>0E
      BYTE  >BA,>91,>FE,>26,>9E,>D8,>E8,>9A
      BYTE  >F9,>8E,>1A,>15,>06,>44,>5D,>46
      BYTE  >80,>C8,>D5,>04,>D0,>B4,>9B,>00
      BYTE  >B2,>72,>57,>40
      BYTE  >B2,>E1,>49,>B4,>26,>2A,>C3,>19
      BYTE  >15,>33,>3A,>8B,>28,>47,>52,>EA
      BYTE  >E4,>34,>CA,>0D,>49,>AA,>B3,>91
      BYTE  >6C,>53,>B4,>0F
*     GOOD DAY
SP15  DATA  170
      BYTE  >A5,>A8,>A3,>23,>D4,>94,>B4,>A2
      BYTE  >96,>22,>37,>77,>73,>CA,>1C,>9C
      BYTE  >25,>23,>F7,>29,>B3,>0F,>96,>8C
      BYTE  >DC,>A7,>AA,>26,>94,>22,>F3,>A6
      BYTE  >DA,>5B,>B4,>B4
      BYTE  >48,>65,>6A,>E5,>8D,>D5,>27,>A1
      BYTE  >6B,>8C,>0C,>13,>0B,>B7,>A1,>52
      BYTE  >56,>BD,>C5,>DD,>A4,>4A,>59,>AB
      BYTE  >20,>75,>D6,>2A,>E5,>C2,>DD,>D8
      BYTE  >D2,>28,>94,>F9
      BYTE  >27,>09,>B7,>27,>F5,>72,>5F,>D8
      BYTE  >9C,>9E,>28,>B0,>79,>61,>73,>7B
      BYTE  >7C,>AF,>6F,>D5,>C8,>CF,>71,>BD
      BYTE  >DB,>B0,>C2,>CC,>C7,>F6,>AD,>D5
      BYTE  >0B,>3B,>1F,>3B
      BYTE  >8C,>12,>2F,>CA,>BD,>EC,>74,>5D
      BYTE  >25,>24,>ED,>F1,>5B,>31,>17,>96
      BYTE  >A6,>27,>1E,>21,>9D,>D4,>6A,>9F
      BYTE  >A2,>FB,>14,>0E,>AF,>B4,>EA,>61
      BYTE  >8D,>25,>2D,>76
      BYTE  >6B,>BB,>52,>F6,>62,>B7,>A9,>F3
      BYTE  >5A,>A4,>4C,>93,>B9,>CE,>58,>F1
      BYTE  >94,>C8,>A2,>06,>4D,>C3,>CB,>22
      BYTE  >CD,>03
      END
