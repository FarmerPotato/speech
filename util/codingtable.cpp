#include <stdint.h>
#include <stdio.h>
#include "tms5110r.hxx"


int main()

{
#define TMS5220 1
#if TMS5220
    const struct tms5100_coeffs* coeffs = &tms5220_coeff;
#else
    const struct tms5100_coeffs* coeffs = &T0285_2501E_coeff;
#endif

    // output scaled ktable
    for (int i=0; i<coeffs->num_k; ++i) {
         printf("    static float k%d[] = {\n        ", i+1);
         int n = 1 << coeffs->kbits[i];
         float m = 1 << (MAX_K-1);
         for (int j=0; j<n; ++j) {
             float c = coeffs->ktable[i][j] / m;
             printf("%.5ff, ", c);
         }
         printf("\n    };\n");
    }
    
    // output pitchtable
    printf("    static float pitch[] = {\n        ");
		 int n = MAX_SCALE;
		 for (int j=0; j<n; ++j) {
				 float c = coeffs->pitchtable[j];
				 printf("%.5ff, ", c);
		 }
		 printf("\n    };\n");
	
    
}
