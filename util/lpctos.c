/**********************************************************************************

	lpctos - Translate comma-separated raw LPC input to assembly source.
	         Optionally, decode the LPC frame by frame.

	         This utility has been tested with CSV files created in Blue Wizard.

           Usage: lpctos [options] inputfile outputfile [reportfile]
           
	   options:
		  -f read filenames from inputfile
	          -b output TI BASIC program source

           inputfile: CSV file of LPC, as output by Blue Wizard.
           outputfile: Assembly source code of the byte values.
           reportfile: Frame-by-frame decoding of the LPC. '-' mean stdout.
           
           
 (c)2020 Erik Olson. No restrictions on use. See LICENSE.

**********************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/errno.h>
#include <sys/stat.h>
#include <unistd.h>

typedef struct {
	int opt_basic; // -b
	int opt_decimal; // -d
	int opt_filelist; // -f
	int lineno;
	int base;

	const char* program_name;
	const char* input_filename;
	const char* output_filename;
	const char* decode_filename;
} Options;

int  parse_options(int argc, char** argv, Options* opts);
unsigned char* read_file(FILE *ifp, size_t len);
char* trim_whitespace(char *str);
void process_file(FILE* ifp, FILE* ofp, FILE* ofp_report, Options* opts);
void process_filelist(FILE* ifp, FILE* ofp, FILE* ofp_report, Options* opts);
void write_assembly(FILE* ofp, unsigned char* buf, int len, Options* opts);
void write_basic(FILE* ofp, unsigned char* buf, int len, Options* opts);
int  parse_csv(unsigned char* buf, int len, unsigned char** poutbuf, int* poutlen, Options* opts);
void decode_lpc(FILE *ofp,  unsigned char* buf, int len, Options* opts);
int  take_bits(unsigned char** ptr, int n, int *pbit);

const char* usage = "Usage: %s [options] inputfile-lpc-csv outputfile-src [outputfile-lpc-decoded]\n"
"        -b            Write BASIC instead of assembly\n"
"        -d            Parse the input as decimal, not hex\n"
"        -n lineno     Starting line number for BASIC, or label in assembly\n";

int parse_options(int argc, char** argv, Options* opts) {
    opts->program_name = argv[0];
	opts->input_filename = NULL;
	opts->output_filename = NULL;
	opts->decode_filename = NULL;
	opts->opt_basic = 0;
	opts->opt_decimal = 0;
	opts->opt_filelist = 0;
	opts->lineno = 0;

    // awkward: -f is not followed by the filelist 
    char* optstring = "bdfn:";
    int nfiles = 0;

    int opt;
    while ((opt = getopt(argc, argv, optstring)) != -1) {
    	switch(opt) {
	case 'b':
		opts->opt_basic = 1;
		break;
	case 'd':
		opts->opt_decimal = 1;
		break;
	case 'n':
		opts->lineno = atoi(optarg);
		break;
	case 'f':
		opts->opt_filelist = 1;
		break;
	default:
		return 0; // caller prints usage
    	}
    }
    nfiles = argc - optind;
    if (nfiles>0) {
    	opts->input_filename = argv[optind];
    }
    if (nfiles>1) {
    	opts->output_filename = argv[1+optind];
    }
    if (nfiles>2) {
    	opts->decode_filename = argv[2+optind];
    }
    if (nfiles>3) {
	fprintf(stderr, "%s: extra argument: \"%s\"\n", argv[3+optind]);
    }
    opts->base = opts->opt_decimal ? 10 : 16;
    if (opts->lineno == 0) {
    	if (opts->opt_basic) {
		opts->lineno = 1000;
	} else {
		opts->lineno = 2;
	}
    }
    
    return nfiles;
}

int main(int argc, char **argv) {
    Options opts;

    if (parse_options(argc, argv, &opts) < 2) {
        fprintf(stderr, usage, opts.program_name);
        exit(1);
    }

    FILE *ifp = fopen(opts.input_filename, "r");
    if (ifp == NULL) {
        fprintf(stderr, "%s: can't read input %s: %s\n", opts.program_name, opts.input_filename, strerror(errno));
        exit(1);
    }

    FILE *ofp = fopen(opts.output_filename, "w");
    if (ofp == NULL) {
        fprintf(stderr, "%s: can't write output %s: %s\n", opts.program_name, opts.output_filename, strerror(errno));
        exit(1);
    }

		/* Optional output file of LPC decode report. '-' means stdout. */
    FILE *ofp_report = NULL;
    if (opts.decode_filename) {
    	if (0==strcmp(opts.decode_filename, "-")) {
    		ofp_report = stdout;
    	} else {
				ofp_report = fopen(opts.decode_filename, "w");
				if (ofp_report == NULL) {
					fprintf(stderr, "%s: can't write output %s: %s\n", opts.program_name, opts.decode_filename, strerror(errno));
					exit(1);
				}
			}
    }

    /* is it a directory? */
    struct stat stats;    
    fstat(fileno(ifp), &stats);
    
    if (stats.st_mode & S_IFDIR) {
    	fprintf(stderr, "%s: Processing directory--not yet implemented\n", argv[0]);
    	exit(1);
    }
    
    if (opts.opt_filelist) {
        process_filelist(ifp, ofp, ofp_report, &opts);
    } else {
        process_file(ifp, ofp, ofp_report, &opts);
    }

    fclose(ifp);
		fclose(ofp);

		if (ofp_report && ofp_report != stdout) {
	    fclose(ofp_report);
	  }
}

unsigned char* read_file(FILE *ifp, size_t len) {
	unsigned char *buf = (unsigned char*)malloc(len+1);
	if (0 == fread(buf, len, 1, ifp)) {
		free(buf);
		buf = NULL;
	} else {
		buf[len] = 0; // make sure it is terminated
	}
	return buf;
}

void process_file(FILE* ifp, FILE* ofp, FILE* ofp_report, Options* opts) {
        /* Get input file size, then load it all. */
        struct stat stats;
        fstat(fileno(ifp), &stats);
	int len = stats.st_size;
	unsigned char *buf = read_file(ifp, len);
	if (!buf) {
		fprintf(stderr, "%s: Error reading input file %s: %s\n", opts->program_name, opts->input_filename, strerror(errno));
		exit(1);
	}    
	
	unsigned char* outbuf = NULL;
	int outlen = 0;
	
	/* csv -> binary -> assembly source, and LPC decoded */
	
	parse_csv(buf, len, &outbuf, &outlen, opts);
	
	
	if (opts->opt_basic) {
		write_basic(ofp, outbuf, outlen, opts);
	} else {
		write_assembly(ofp, outbuf, outlen, opts);
	}

	if (ofp_report) {
		decode_lpc(ofp_report, outbuf, outlen, opts);
	}
	
	if (buf) {
		free(buf);
	}
	if (outbuf) {
		free(outbuf);
	}
}

char* trim_whitespace(char *str) {
    // trim leading whitespace
    while(isspace((unsigned char)*str)) str++;
    if (*str == 0) {
        return NULL;
    } // empty string
    // trim trailing whitespace
    char *pend = str + strlen(str) - 1;
    while(pend > str && isspace(*pend)) pend--;
    pend[1] = '\0';
    return str;
}

/* Process a list of files  from -f */
void process_filelist(FILE* ifp, FILE* ofp, FILE* ofp_report, Options* opts) {
	char buf[80];
	while(fgets(&buf[0], sizeof(buf), ifp)) {
	    opts->input_filename = trim_whitespace(buf);
	    if (opts->input_filename == NULL) {
	    	continue;  // blank line
	    }

	    /* process the named file */
	    FILE *ifp2 = fopen(opts->input_filename, "r");
	    if (ifp2 == NULL) {
		fprintf(stderr, "%s: can't read input %s: %s\n", opts->program_name, opts->input_filename, strerror(errno));
		exit(1);
	    } else {
                process_file(ifp2, ofp, ofp_report, opts);
	    }
	}
}

/* Assuming the buffer is comma-separated values, parse it into binary.
   len is the number of input chars, not the final length.
   Output buffer is allocated and returned in *poutbuf.
   Return value is length of outbuf.
*/

int parse_csv(unsigned char* buf, int len, unsigned char** poutbuf, int* poutlen, Options* opts) {
  unsigned char* outbuf = (unsigned char*)malloc(len); // plenty
  unsigned char* ptr = buf;
  unsigned char* ptr2 = buf;
  int  outlen = 0;
  
  fprintf(stderr, "%-20s: Parsing csv file, length %d, base %d\n", opts->input_filename, len, opts->base);

  do {
		int val = strtol((char*)ptr, (char**)&ptr2, opts->base);
		if (ptr == ptr2) {
			fprintf(stderr, "%-20s: Stopping at index %d char \\%x\n", opts->input_filename, (int)(ptr2-buf), *ptr2);
			break;
		}

		ptr = ptr2;
		if (*ptr == ',') {
			++ptr;
		}

		outbuf[outlen++] = val;
  } while(*ptr2 != 0);
  
  if (poutbuf) {
		*poutbuf = outbuf;
	}
  if (poutlen) {
		*poutlen = outlen;
	}
  return outlen;
}


/* Assuming the buffer is binary data, write 8 bytes per line.
   len is the number of binary bytes.
   In assembly mode, does not include a speak external or a stop command.
   In XB mode, insert the start command and length. The stop command is up
   to you. In BlueWizard, check the box for 'include stop command.'
   Appending FF is unreliable.
*/

void write_assembly(FILE* ofp, unsigned char* buf, int len, Options* opts) {
	/* Header is the way OTALK likes it. */
	
  fprintf(stderr, "%-20s: Writing %d bytes to assembly source file at label SP%d.\n", opts->output_filename, len, opts->lineno);
  fprintf(ofp, "*\n*      %s\n*\n", opts->input_filename);
  fprintf(ofp, "%6s EVEN\n", "");
  // OTALK has labels SP1 (built-in) SP2 external...
	fprintf(ofp, "SP%-4d DATA %d        * length", opts->lineno++, len);
	for (int i=0; i<len; ++i) {
		if(i%8 == 0) {
			fprintf(ofp, "\n%6s BYTE >%02x", "", buf[i]);
		} else {
			fprintf(ofp, ",>%02x", buf[i]);
		}
	}
	fprintf(ofp, "\n");		
}

void write_basic(FILE* ofp, unsigned char* buf, int len, Options* opts) {
  fprintf(stderr, "%-20s: Writing %d bytes to BASIC listing file at line number %d.\n", opts->output_filename, len, opts->lineno);
  const int start_cmd = 0x60;
  const int lobyte = len & 0xff;
  const int hibyte = len >> 8;
	static int index = 1;
	fprintf(ofp, "%d REM %s\n", opts->lineno, opts->input_filename);
	fprintf(ofp, "%d DATA %d !LENGTH\n%d DATA %d,%d,%d\n", opts->lineno, len+3, opts->lineno+10, start_cmd, hibyte, lobyte);
	opts->lineno += 20;
	char linebuf[128];
	int  linelen=0;
	for (int i=0; i<len; ++i) {
		if(i%20 == 0 && i!=len-1) {
			// flush and start a new line;
			if (i!=0) {
				linebuf[linelen] = 0;
				fprintf(ofp, "%s\n", linebuf);
			}
			linelen = 0;
			linelen += sprintf(linebuf+linelen, "%d DATA %d", opts->lineno, buf[i]);
			opts->lineno += 10;
		} else {
			linelen += sprintf(linebuf+linelen, ",%d", buf[i]);		
		}
	}
	linebuf[linelen] = 0;
	if (linelen>0) {
		fprintf(ofp, "%s\n", linebuf);
	}
}

/* Decode the LPC data frame by frame. 
              Energy Rpt Pitch   K1  K2  K3  K4  K5  K6  K7  K8  K9  K10  Total
         Bits    4    1    6      5   5   4   4   4   4   4   3   3   3    50
         
       Voiced  eeee   0  pppppp   5   5   4   4   4   4   4   3   3   3    50
     Unvoiced  eeee   0  000000   5   5   4   4                            29
       Repeat  eeee   1  pppppp                                            11
  Zero Energy  0000                                                         4
       Stop    1111                                                         4

  If clk is 8Mhz, frame is 25 ms, then 40 Hz and 2000 bps of voiced speech.

Example:
  
*    175     7    3  0  voiced   10   0   39   23 16  4  9  4 11 10  6  5  1
*    200     8    9  2  voiced   13   0   40   24 15  4  9  5 10 10  5  5  2
*    225     9   15  4  voiced   14   0   39   24 16  4  9  6  9  9  5  5  2

Reference:

	TMS5220 Voice Synthesis Processor (VSP) Data Manual

	http://bitsavers.org/components/ti/_dataBooks/TMS5220_Voice_Synthesis_Processor_Data_Manual_-_preliminary_Jun81.pdf

	http://ftp.whtech.com/datasheets%20and%20manuals/Datasheets%20-%20TI/TMC%200285%20Speech%20Synthesis%20Processor.pdf

*/


void decode_lpc(FILE *ofp,  unsigned char* buf, int len, Options* opts) {
	fprintf(stderr, "%-20s: Writing decoded LPC frame-by-frame.\n", opts->decode_filename);
	
  unsigned char* ptr = buf;
  int bit = 0; // bit index in byte. take from LSbit first.

  int energy = 0;
  int repeat = 0;
  int pitch = 0;
  int k[10];
  int kbits[10] = { 5, 5, 4, 4, 4, 4, 4, 3, 3, 3 };
  int stop = 0;
  int frame = 0;
  int ms = 25;   // ms per frame
  
  // Report columns widths:
  // time frame addr bit type energy rpt pitch k1 k2 k3 k4 k5 k6 k7 k8 k9 k10
  // %6s %5s %4s %3s %4s %6s %3s %5s %2s %2s %2s %2s %2s %2s %2s %2s %2s %2s
  // %6d %5d %4d %3d %8s %2d %3d %5d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d
  
  fprintf(ofp, "*\n* %s\n*\n", opts->input_filename);
  fprintf(ofp, "* %6s %-5s %4s %3s %-4s %-7s %3s %5s k1 k2 k3 k4 k5 k6 k7 k8 k9 k10\n", "time", "frame", "addr", "bit", "type", "energy", "rpt", "pitch"); // header
  
  while(ptr < buf+len && !stop) {
  	++frame;
  	fprintf(ofp, "* %6d %5d %4d %2d  ", (frame-1)*ms, frame, (int)(ptr-buf), bit);
    int energy = take_bits(&ptr, 4, &bit);
    if (energy == 15) {
      fprintf(ofp, "%-8s %2d\n", "stop", energy);
      stop = 1;
    } else if (energy == 0) {
      fprintf(ofp, "%-8s %2d\n", "zero", energy);
    } else {
    	repeat = take_bits(&ptr, 1, &bit);
    	pitch = take_bits(&ptr, 6, &bit);
  		if (repeat) {
        fprintf(ofp, "%-8s %2d %3d %4d \n", "repeat", energy, repeat, pitch);
      } else {
        for (int ki=0; (pitch>0 && ki<10) || (pitch==0 && ki<4); ++ki) {
        	k[ki] = take_bits(&ptr, kbits[ki], &bit);
        }
        
      	if (pitch == 0) {
					fprintf(ofp, "%-8s %2d %3d %4d   %2d %2d %2d %2d\n", "unvoiced", energy, repeat, pitch, k[0], k[1], k[2], k[3]);
				} else {
					fprintf(ofp, "%-8s %2d %3d %4d   %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d\n", "voiced", energy, repeat, pitch, k[0], k[1], k[2], k[3], k[4], k[5], k[6], k[7], k[8], k[9]);
				}
			}
    }
  }
  if (ptr >= buf+len && !stop) {
		fprintf(stderr, "Warning: no stop frame was seen before all bits were used\n");
	}
	
	/* Bits per second */
	double bits = (ptr-buf)*8 + bit;
	double bps = bits / frame * 1000 / ms;
	fprintf(ofp, "* BITS/SEC: %.1f\n", bps);
}


/* take n bits, from pbit up to pbit+n-1 */
/* *pbit [7:0] */
/* always returns with byte replenished */
int take_bits(unsigned char** ptr, int n, int *pbit) {
  unsigned char masks[8] = { 0x1, 0x2, 0x4, 0x8, 0x10, 0x20, 0x40, 0x80 };
//  unsigned char masks[8] = { 0x80, 0x40, 0x20, 0x10, 0x8, 0x4, 0x2, 0x1 };

  int x = 0;

  int i=0;
  do {
    /* loop through available bits */
		for (; i<n && *pbit<8; (*pbit)++, i++) {
		  // get one bit
			int bit = (**ptr) & masks[*pbit] ? 1 : 0;
			x = (x<<1) + bit;
		}

		/* if byte is exhausted, replenish bits */
		if (*pbit == 8) {
			++(*ptr);
			*pbit = 0;
		}

	} while(i<n); // continue in new byte

	return x;
}
